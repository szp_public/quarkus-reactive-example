# quarkus-reactive-example project

## Purpose

The purpose of this project is to introduce 2 main aspects:
1. Consuming reactively other API's with different parameters coming from a list with retry e fallback features (e.g. `TzQueryResource`);
1. Dealing with bulkhead pattern with asynchronous service with retry and fallback features; 

### 1. Query repeatedly an external API in a reactive way


#### TL;DR
Entrypoint: see `src/test/java/szp/rafael/qre/tzquery/TZQueryResourceTest.java`

Rest Resource: see `src/main/java/szp/rafael/qre/tzquery/TZQueryResource.java`. In this service there is a  conversion from reactive/async to sync/blocking.

Retry and Fallback config: see `src/main/java/szp/rafael/qre/worldTimeApi/WorldTimeApiService.java`

Consuming an external API reactively with multiny: see `src/main/java/szp/rafael/qre/tzquery/TzQueryService.java`. Attention: there is throttling/delay so the destination doesn't get overloaded

#### Concept applied

The purpose of this example is to demonstrate how to consume an external service which can be faulty with consecutive calls. 
The best approach is to have a random delay for every call. Other important feature is that every call needs do be processed independently of each other. 
However, using local Threads, Futures and/or ExecutorService is not encouraged nowadays, so I chose mutiny, which is a native  reactive quarkus framework 
so every call can be processed in a fancy way.

Another feature is that the Time API service is kind of faulty, in other words, it fails a lot, so every call needs to be retried at least once, but not indefinitely. At last there must be a maximum duration for all the retries combined. 

Therefore, if the service fails even using the retry strategy, a fallback must be provided so client knows that one of those calls failed. In this case I chose to send an EMPTY object, but a cached response can be used instead. 


### 2. Dealing with bulkhead pattern in an asynchronous way

#### TL;DR;

Entrypoint: see `szp/rafael/qre/slow/SlowResourceTest.java`

Rest Resource: see `src/main/java/szp/rafael/qre/slow/SlowResource.java`. Notice that the `@Asynchronous`, `@Bulkhead`, `@Fallback` annotations can work independently of each other. But `@Retry` will only work only if @Asynchronous annotation is present.  

#### Concept applied

The purpose of this example is to demonstrate how to limit a number of concurrent calls that a resource/endpoint can handle, this technique is called Bulkhead. In this case I used a waiting task queue, which is the maximum number of requests in the queue,
so if this queue is exhausted the service would send a status 500 to the origin/client when there is no fallback feature.

Besides, even if there is a @Retry annotation the @Asynchonous is needed as well so every failing requests can be retried for a **maxRetries**  with a delay between each other.

Another feature used in this example is the fallback pattern, which redirects all failing requests to a class or method that would handle failed all requests fail due to exhaustion of the resource/endpoint.

## Appendix

1. Quarkus getting started with reactive: https://quarkus.io/guides/getting-started-reactive
1. Quarkus Reactive with mutiny: https://redhat-developer-demos.github.io/quarkus-tutorial/quarkus-tutorial/reactive.html
1. Quarkus cheat sheet: https://lordofthejars.github.io/quarkus-cheat-sheet/
1. Smallrye Mutiny, first lines of code: https://smallrye.io/smallrye-mutiny/getting-started/first-lines-of-code
1. Smallrye Mutiny, Guides: https://smallrye.io/smallrye-mutiny/guides
1. Microprofile fault tolerance: https://download.eclipse.org/microprofile/microprofile-fault-tolerance-1.1.2/microprofile-fault-tolerance-spec.html
1. Microprofile Rest client: https://download.eclipse.org/microprofile/microprofile-rest-client-2.0-RC2/microprofile-rest-client-2.0-RC2.html

## TODO
1. Paginated reactive results based on this tutorial: https://redhat-developer-demos.github.io/quarkus-tutorial/quarkus-tutorial/reactive.html
1. Merging and concatenating results from different services in order to build a DTO comprising all data needed;
1. Data streaming with Redis/Kafka;
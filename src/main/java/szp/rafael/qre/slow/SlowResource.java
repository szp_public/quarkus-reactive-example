package szp.rafael.qre.slow;

import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.faulttolerance.Asynchronous;
import org.eclipse.microprofile.faulttolerance.Bulkhead;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.concurrent.CompletionStage;

@Path("/slow")
public class SlowResource {

  @Inject
  SlowService service;

  @Asynchronous
  @Bulkhead(value=2, waitingTaskQueue = 20)
  @Fallback(SlowFallback.class)
  @Retry(maxRetries = 10, delay = 501)
  @GET
  public CompletionStage<Integer> getRandom(){
    return Uni.createFrom().item(service.verySlowService()).subscribeAsCompletionStage();
  }
}

package szp.rafael.qre.slow;

import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

import javax.inject.Singleton;
import java.util.concurrent.CompletionStage;

@Singleton
public class SlowFallback implements FallbackHandler<CompletionStage<Integer>> {

  @Override
  public CompletionStage<Integer> handle(ExecutionContext executionContext) {
    return Uni.createFrom().item(Integer.valueOf(0)).subscribeAsCompletionStage();
  }
}
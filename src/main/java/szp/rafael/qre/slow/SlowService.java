package szp.rafael.qre.slow;

import javax.inject.Singleton;
import java.util.Random;

@Singleton
public class SlowService {

  Random random =new Random();

  public Integer verySlowService(){
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
    }
    return random.nextInt(10_000)+1;
  }

}

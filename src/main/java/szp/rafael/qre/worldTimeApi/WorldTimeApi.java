package szp.rafael.qre.worldTimeApi;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class WorldTimeApi {

  private String abbreviation;
  @JsonProperty(value = "client_ip")
  private String clientIp;

  private ZonedDateTime datetime;

  @JsonProperty(value = "day_of_week")
  private Integer dayOfWeek;

  @JsonProperty(value = "day_of_year")
  private Integer dayOfYear;

  private Boolean dst;

  @JsonProperty(value = "dst_from")
  private String dstFrom;

  @JsonProperty(value = "dst_offset")
  private Integer dstOffset;

  @JsonProperty(value = "dst_until")
  private String dstUntil;

  @JsonProperty(value = "raw_offset")
  private Integer rawOffset;

  private String timezone;

  private Long unixtime;
  @JsonProperty(value = "utc_datetime")
  private ZonedDateTime utcDatetime;

  @JsonProperty(value = "utc_offset")
  private String utcOffset;

  @JsonProperty(value = "week_number")
  private Integer weekNumber;


  public WorldTimeApi() {
  }

  public WorldTimeApi(String abbreviation, String clientIp, ZonedDateTime datetime, Integer dayOfWeek, Integer dayOfYear, Boolean dst, String dstFrom, Integer dstOffset, String dstUntil, Integer rawOffset, String timezone, Long unixtime, ZonedDateTime utcDatetime, String utcOffset, Integer weekNumber) {
    this.abbreviation = abbreviation;
    this.clientIp = clientIp;
    this.datetime = datetime;
    this.dayOfWeek = dayOfWeek;
    this.dayOfYear = dayOfYear;
    this.dst = dst;
    this.dstFrom = dstFrom;
    this.dstOffset = dstOffset;
    this.dstUntil = dstUntil;
    this.rawOffset = rawOffset;
    this.timezone = timezone;
    this.unixtime = unixtime;
    this.utcDatetime = utcDatetime;
    this.utcOffset = utcOffset;
    this.weekNumber = weekNumber;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public ZonedDateTime getDatetime() {
    return datetime;
  }

  public void setDatetime(ZonedDateTime datetime) {
    this.datetime = datetime;
  }

  public Integer getDayOfWeek() {
    return dayOfWeek;
  }

  public void setDayOfWeek(Integer dayOfWeek) {
    this.dayOfWeek = dayOfWeek;
  }

  public Integer getDayOfYear() {
    return dayOfYear;
  }

  public void setDayOfYear(Integer dayOfYear) {
    this.dayOfYear = dayOfYear;
  }

  public Boolean getDst() {
    return dst;
  }

  public void setDst(Boolean dst) {
    this.dst = dst;
  }

  public String getDstFrom() {
    return dstFrom;
  }

  public void setDstFrom(String dstFrom) {
    this.dstFrom = dstFrom;
  }

  public Integer getDstOffset() {
    return dstOffset;
  }

  public void setDstOffset(Integer dstOffset) {
    this.dstOffset = dstOffset;
  }

  public String getDstUntil() {
    return dstUntil;
  }

  public void setDstUntil(String dstUntil) {
    this.dstUntil = dstUntil;
  }

  public Integer getRawOffset() {
    return rawOffset;
  }

  public void setRawOffset(Integer rawOffset) {
    this.rawOffset = rawOffset;
  }

  public String getTimezone() {
    return timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }

  public Long getUnixtime() {
    return unixtime;
  }

  public void setUnixtime(Long unixtime) {
    this.unixtime = unixtime;
  }

  public ZonedDateTime getUtcDatetime() {
    return utcDatetime;
  }

  public void setUtcDatetime(ZonedDateTime utcDatetime) {
    this.utcDatetime = utcDatetime;
  }

  public String getUtcOffset() {
    return utcOffset;
  }

  public void setUtcOffset(String utcOffset) {
    this.utcOffset = utcOffset;
  }

  public Integer getWeekNumber() {
    return weekNumber;
  }

  public void setWeekNumber(Integer weekNumber) {
    this.weekNumber = weekNumber;
  }
}

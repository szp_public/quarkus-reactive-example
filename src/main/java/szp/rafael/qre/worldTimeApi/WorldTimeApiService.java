package szp.rafael.qre.worldTimeApi;


import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;

@Path("/api")
@RegisterRestClient
public interface WorldTimeApiService {

  @GET
  @Path("/timezone/{area}")
  @Produces(MediaType.APPLICATION_JSON)
  @Retry(maxRetries = 3, delay = 100,maxDuration = 30_000L)
  @Fallback(WorldTimeApiServiceFallback.class)
  WorldTimeApi area(@PathParam("area") String area);

  class WorldTimeApiServiceFallback implements FallbackHandler<WorldTimeApi>{

    static WorldTimeApi EMPTY_TZ = new WorldTimeApi();
    @Override
    public WorldTimeApi handle(ExecutionContext executionContext) {
      EMPTY_TZ.setTimezone("ERROR");
      EMPTY_TZ.setAbbreviation(Arrays.asList(executionContext.getParameters()).toString());
      return EMPTY_TZ;
    }
  }

}

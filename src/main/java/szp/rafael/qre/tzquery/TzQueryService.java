package szp.rafael.qre.tzquery;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import szp.rafael.qre.worldTimeApi.WorldTimeApi;
import szp.rafael.qre.worldTimeApi.WorldTimeApiService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Duration;
import java.util.List;
import java.util.Random;

@Singleton
public class TzQueryService {

  @RestClient
  @Inject
  WorldTimeApiService timeApi;

  public Multi<WorldTimeApi> queryFromList(List<String> list) {
    Random random = new Random();
    return Multi.createBy().repeating()
      .supplier(
        () -> list.iterator(),
        it -> it.hasNext() ? it.next() : ""
      ).until(s -> s.equals(""))
      .onItem().call(i -> {
        Duration delay = Duration.ofMillis(random.nextInt(500) + 1); //Throttling so the endpoint is not overloaded
        return Uni.createFrom().nullItem().onItem().delayIt().by(delay);
      })
      .onItem().transformToUniAndConcatenate(item -> Uni.createFrom().item(timeApi.area(item))); //Consuming time API
  }
}

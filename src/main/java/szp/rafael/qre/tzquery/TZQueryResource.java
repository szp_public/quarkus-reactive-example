package szp.rafael.qre.tzquery;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import szp.rafael.qre.worldTimeApi.WorldTimeApi;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;

@Path("/tz-query")
public class TZQueryResource {

  @ConfigProperty(name = "tzquery.tz-br")
  public List<String> brTzs;

  Logger logger = Logger.getLogger(TZQueryResource.class);

  @Inject
  TzQueryService service;

  @GET
  @Path("/list/br")
  public List<WorldTimeApi> fromBr() {
    return service.queryFromList(brTzs)
      .collect().asList()
      .await().indefinitely();
  }


}
package szp.rafael.qre.slow;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
class SlowResourceTest {

  @Test
  void should_get_random() {
    Integer response = given().when()
      .get("/slow")
      .then()
      .statusCode(200)
      .extract().as(Integer.class);
    assertTrue(response >= 0);
  }

}
package szp.rafael.qre.tzquery;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class TZQueryResourceTest {

    @ConfigProperty(name = "tzquery.tz-br")
    public List<String> brTzs;

    @Test
    public void should_get_tz_from_predefined_list(){
        Response response = given()
          .when().get("/tz-query/list/br")
          .then().statusCode(200)
          .body("timezone",hasItems(brTzs.toArray(new String[brTzs.size()])))
          .extract().response();
        System.out.println(response.asPrettyString());
    }

}
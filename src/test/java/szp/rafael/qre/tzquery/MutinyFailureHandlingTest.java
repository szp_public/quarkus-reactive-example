package szp.rafael.qre.tzquery;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class MutinyFailureHandlingTest {


  Integer intermitentService(int i){
    if(i==7){
      throw new RuntimeException();
    }
    return i*i;
  }

  Uni<Integer> intermitentUni(int i){
    return Uni.createFrom().item(i).onItem().invoke(ii->{
      if(ii==7) throw new RuntimeException();
    }).map(ii-> ii*ii);
  }

  @Test
  void should_handle_non_multi_errors(){
    IntStream list = IntStream.range(1,11);
    Multi.createBy().repeating()
      .supplier(
        ()->list.iterator(),
        it -> it.hasNext()? it.next():-1
      ).until(item->item==-1)
      .map(i-> {
        Integer r = -1;
        try {
          r = intermitentService(i);
        } catch (Exception e) {

        }
        return r;
      })
      .subscribe().with(
        i->System.out.println(i),
        failure-> System.err.println(failure)
      );
  }

  @Test
  void should_handle_multi_errors(){
    IntStream list = IntStream.range(1,11);
    Multi.createBy().repeating()
      .supplier(
        ()->list.iterator(),
        it -> it.hasNext()? it.next():-1
      ).until(item->item==-1)
      .map(i-> intermitentUni(i).onFailure().recoverWithItem(0).await().indefinitely())
      .subscribe().with(
      i->System.out.println(i),
      failure-> System.err.println(failure)
    );
  }

}
